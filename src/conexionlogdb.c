#include <stdio.h>
#include <sys/types.h>         
#include <sys/stat.h>
#include <stdio.h>             
#include <stdlib.h>            
#include <stddef.h>             
#include <string.h>           
#include <unistd.h>             
#include <signal.h>         
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

#include "logdb.h"

#define LIMITE 100

conexionlogdb *conectar_db(char *ip, int puerto){

	conexionlogdb *conexion = malloc(sizeof(conexion)); 
	conexion->id_sesion = 0;
	conexion->nombredb = (char *)malloc(50); 
	
	
	struct sockaddr_in direccion;
	memset(&direccion, 0, sizeof(direccion));	
	direccion.sin_family = AF_INET;		
	direccion.sin_port = htons(puerto);		
	direccion.sin_addr.s_addr = inet_addr(ip);
	
	int numsec, fd; 

	for (numsec = 1; numsec <= LIMITE; numsec <<= 1) { 

		if ((fd = socket(direccion.sin_family, SOCK_STREAM, 0)) < 0) {
			return(NULL); 
		}

		if (connect(fd, (struct sockaddr *)&direccion, sizeof(direccion)) == 0) {
			conexion->ip = ip;
			conexion->puerto = puerto;
			conexion->sockdf = fd;
			conexion->id_sesion = rand()%(500) + 0;
			
			return conexion; 
		} 
		close(fd); 			
	} 

	return NULL; 
}
