#include <stdio.h>
#include <sys/socket.h>

#include "logdb.h"

void cerrar_db(conexionlogdb *conexion){

	if(conexion == NULL){
		return ;
	}

	//No se ha abierto la DB
	if(strcmp(conexion->nombredb, "") == 0){
		return ;
	}

	//Enviar al servidor 
	char *mensaje = (char *)malloc(50); 
	strcpy(mensaje,conexion->nombredb);
	char* cerrar=",cerrar";
 	strncat(mensaje, cerrar,strlen(cerrar)); 
	int p = 0;
	p = send(conexion->sockdf, mensaje, 50,0); 
	if (p < 0) {
		printf("Send error Cliente\n"); 
		return ;
	}


	//Respuesta del servidor
	int n = 0;
	char buf[100] = "";
	char *mensajeok= "ok";
	n = recv(conexion->sockdf, buf, 100, 0);
	
	printf("\n CERRAR recibe :  > %s\n",buf); //recibo ok

	if (n < 0){
		printf("Recv error Cliente\n"); 
		return ;
	}	
	if (strcmp(buf,mensajeok) == 0) {
		close(conexion->sockdf);
		return ;	
	}

	return ;

}
