#include <stdio.h>
#include <fcntl.h>
#include <sys/socket.h>

#include "hashtable.h"
#include "indice.h"
#include "logdb.h"

#define TAMANO 100

char **claves_arr;
char **valores_arr;

int put_val(conexionlogdb *conexion, char *clave, char *valor){

	if(conexion == NULL || clave == NULL || clave == valor){
		return 0;
	}

	//No se ha abierto la DB
	if(strcmp(conexion->nombredb, "") == 0){
		return 0;
	}

	//Se realiza el put en la base y luego de guardar se actualiza el indice
	//Enviar al servidor
	char *mensaje = (char *)malloc(50); 
	strcpy(mensaje,conexion->nombredb);
	char* put=",put,"; 
 	strncat(mensaje, put,strlen(put));
	char* cl=clave; 
 	strncat(mensaje, cl,strlen(cl));
	strcat(mensaje, ",");
	char* vl=valor; 
 	strncat(mensaje, vl,strlen(vl));
	
	int p = 0;
	p = send(conexion->sockdf, mensaje, 50,0); 
	if (p < 0) {
		printf("Send error Cliente\n"); 
		return 0;
	}

	//Respuesta del servidor
	int n = 0;
	char buf[100] = "";
	char *mensajeok = "ok";
	n = recv(conexion->sockdf, buf, 100, 0);
	printf("\n PUT recibe :  > %s\n",buf); //Recibo ok
	if (n < 0){
		printf("Recv error Cliente\n"); 
		return 0;
	}	
	if (strcmp(buf,mensajeok) == 0) {

		int fd = open("./config", O_RDWR | O_APPEND, S_IRWXU);
		if(fd <= 0){
			perror("No se pudo abrir el archivo de configuracion\n");
			return 0;	
		}

		char leer; char linea[200] = ""; int r = 0;  int indice = 0;

		//Lee línea a línea y escribe en pantalla hasta el fin de fichero
		while(read(fd,&leer,1) != 0) { 
			linea[r] = leer; 
			r++;

			if(leer == '\n'){
				char *nombreBase = (char *)malloc(50);
				char *indiceDirec = (char *)malloc(50);
				char* ch = strtok(linea, ",");
				indice ++;
				int i=0;

				while (ch != NULL) {
					if(i==0){
						nombreBase = ch;
					} 
					if(i==2){
						indiceDirec = ch;
					} 
					i++;
					ch = strtok(NULL, ",");
				} 
				if(strcmp(conexion->nombredb,nombreBase)==0){
					close(fd);
					getIndice(nombreBase, indiceDirec);
					return 1;
		
				} else {
					r = 0;
					memset(linea,0,200);
				}

			}		

		}
		close(fd);
		return 1;	
	}

	return (0);
	
}
