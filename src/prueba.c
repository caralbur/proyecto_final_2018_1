/**
 * Autor: Eduardo Murillo, M.Sc.
 * 
 **/
#include "hashtable.h"
#include "logdb.h"

int main(int argc, char** argv){

	if(argc<2){
		printf("La cantidad de argumentos es invalida\n");
		exit(-1);
	}
	char *ip;
	ip = argv[1];

	char *puerto;
	puerto = argv[2];
	
	conexionlogdb *conexion = conectar_db(ip, atoi(puerto));
	int c = crear_db(conexion, "t.txt"); printf("CREAR1 %d\n", c); //solo de prueba

	int p1 = put_val(conexion, "ayudantia","gestion"); printf("PUT1 %d\n", p1); //solo de prueba (NO SALDRA -> DB no abierta)
	int e1 = eliminar(conexion, "estudios"); printf("ELIMINAR1 %d\n", e1); //solo de prueba (NO SALDRA -> DB no abierta)
	cerrar_db(conexion);//solo de prueba (NO SALDRA -> DB no abierta)

	int a = abrir_db(conexion, "testfile_10000"); printf("ABRIR %d\n", a); //solo de prueba
	int p2 = put_val(conexion, "chao","sistemas"); printf("PUT2 %d\n", p2); //solo de prueba
	int e2 = eliminar(conexion, "hola"); printf("ELIMINAR2 %d\n", e2); //solo de prueba
	int e3 = eliminar(conexion, "chao"); printf("ELIMINAR3 %d\n", e3); //solo de prueba
	//get_val(conexion, "hola");//prueba
	int p3 = put_val(conexion, "fin","semestre"); printf("PUT3 %d\n", p3); //solo de prueba

	compactar(conexion); //solo de prueba

	int p5 = put_val(conexion, "fin","alslkas"); printf("PUT5 %d\n", p5); //solo de prueba
	int p6 = put_val(conexion, "decimates","alslkas"); printf("PUT6 %d\n", p6); //solo de prueba
	int p7 = put_val(conexion, "infinitating","sfalslkas"); printf("PUT7 %d\n", p7); //solo de prueba
	int p8 = put_val(conexion, "infinitating","adfdgfsb"); printf("PUT8 %d\n", p8); //solo de prueba
	
	cerrar_db(conexion); //solo de prueba

	return 0;
}





