#include <stdio.h>
#include "hashtable.h"

void *remover(hashtable *tabla, char *clave){
	if(tabla == NULL || clave == NULL){ //Verificar que no se pase como argumento un hashtable o una clave nula
		return NULL;
	} else {
		
		unsigned char *key = (unsigned char *) clave;
		unsigned long posicionClave = hash(key)%tabla->numeroBuckets; 
		objeto *obj = tabla->buckets[posicionClave];
		objeto *prev = NULL;	

		while(obj->clave != NULL){ 
			if(strcmp(clave, obj->clave) == 0){
				void *value = strdup(obj->valor);	
				
				if(obj->siguiente != NULL && prev != NULL){ //n-esimo elemento de un bucket con varios elementos
					prev->siguiente = obj->siguiente;
					obj->siguiente = NULL;
				} else if (obj->siguiente == NULL && prev != NULL){ //Ultimo elemento de un bucket con varios elementos
					prev->siguiente = NULL;
				} else if (obj->siguiente == NULL && prev == NULL){ //Elemento unico en el bucket (no tiene previo)
					free(tabla->buckets[posicionClave]);
					tabla->buckets[posicionClave] = (void *)malloc(sizeof(objeto));
				} else if (obj->siguiente != NULL && prev == NULL){ //Primer elemento en un bucket de varios elementos
					tabla->buckets[posicionClave] = obj->siguiente;
					obj->siguiente = NULL;
				}				
				tabla->elementos--; //Al remover un objeto, se reducen los elementos del hashtable
				free(obj->clave);
				free(obj->valor);
				
				return value;
			}
			prev = obj;
			obj = obj->siguiente;
		} 
	}

	return NULL;
}
