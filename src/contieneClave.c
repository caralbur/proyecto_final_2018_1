#include <stdio.h>
#include "hashtable.h"

int contieneClave(hashtable *tabla, char *clave){

	if(tabla == NULL || clave == NULL){ //Verificar que no se pase como argumento un hashtable o una clave nula
		return 0;
	} else {

		unsigned char *key = (unsigned char *) clave;
		unsigned long posicionClave = hash(key)%tabla->numeroBuckets; //Obtener la posicion de la clave (asi se obtendra el bucket)
		objeto *obj = tabla->buckets[posicionClave];
		
		//Recorrer la lista de objetos en el bucket de la posicionClave
		while(obj->clave != NULL){ 
			if(strcmp(clave, obj->clave) == 0){
				return 1;
			}
			obj = obj->siguiente;
		}
	}

	return 0; //Si ni siquiera se realiza una comparacion entonces retornara 0
}
