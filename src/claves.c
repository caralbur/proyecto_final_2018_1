#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

char **claves(hashtable *tabla, int *conteo){
if(tabla == NULL){
		return NULL;
	} else {
		*conteo = 0;
		char **arregloClaves = malloc(sizeof(void *)*(tabla->elementos)); 

		int i = 0;

		while(i < tabla->numeroBuckets){ 
			objeto *obj = tabla->buckets[i];			
			if(obj != NULL && obj->clave != NULL){
				while(obj->siguiente != NULL){
					if(obj->siguiente != NULL && obj->clave != NULL){ 
						arregloClaves[*(conteo)] = obj->clave;					
						*conteo = *conteo + 1;					
					}
					obj = obj->siguiente;						
				}	
			}
			i++;
		}

		//int j = 0;
		//printf("\nLas Claves son: \n\n");
		/*
		while(j < *conteo){
			printf("%s\n",(char *) arregloClaves[j]); 
			j++;
		}
		*/

		printf("\nVariable conteo: %d\n", *conteo);		
		
		return arregloClaves; 
	}

}
