#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

#include "hashtable.h"

#define LINEA 256
#define ARREGLO 2
#define TAMANO 200

hashtable *llenarTabla(char *archivo);

int getIndice(char* nombreDB, char* indice){

	hashtable *tabla = llenarTabla(nombreDB);
	if(tabla == NULL){
		return 0;
	}

	int fd = open(indice, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);

	int i = 0;
	while(i < tabla->numeroBuckets){ 
		objeto *obj = tabla->buckets[i];			
		if(obj != NULL && obj->clave != NULL){
			while(obj->siguiente != NULL){ 
				if(obj->siguiente != NULL && obj->clave != NULL){ 
					char *c = obj->clave;
					write(fd, c, strlen(c));				
					write(fd, ":", strlen(":"));
					char *v = obj->valor;				
					write(fd, v, strlen(v));
					write(fd, "\n", strlen("\n"));
				}
				obj = obj->siguiente;						
			}	
		}
		i++;
	}
	close(fd);
	return 1;	

}


hashtable *llenarTabla(char *archivo){
	
	hashtable *tabla = crearHashTable(1000); 
	
	int fd = open(archivo, O_RDONLY, S_IRWXU);
	char leer; char linea[200] = ""; int r = 0;  int indice = 0;
	
	int posicion = 0;
	int m;
	while((m = read(fd,&leer,1)) != 0) { 
		linea[r] = leer; 
		r++;
		
		if(leer == '\n'){
			char *clave = (char *)malloc(50);
			char *valor = (char *)malloc(50);
			char* ch = strtok(linea, ":");
			indice ++;
			int j = 0;

			while (ch != NULL) {
				if(j == 0){
					clave = ch;
				} 
				if(j == 1){
					valor = ch;
				} 
				j++;
				ch = strtok(NULL, ":");
			} 
			
			char *conve = (char *)malloc(50); 
			sprintf(conve, "%d", posicion);
			
			put(tabla, strdup(clave), conve);

			posicion = posicion+strlen(clave)+strlen(valor)+1;
		
			r = 0;
			memset(linea,0,200);
		}
	}

	lseek(fd,0,SEEK_SET); close(fd);

	return tabla;
}
