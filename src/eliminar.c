#include <stdio.h>
#include <fcntl.h>
#include <sys/socket.h>

#include "hashtable.h"
#include "indice.h"
#include "logdb.h"

int eliminar(conexionlogdb *conexion, char *clave){

	if(conexion == NULL || clave == NULL){
		return 0;
	}

	//No se ha abierto la DB
	if(strcmp(conexion->nombredb, "") == 0){
		return 0;
	}
	
	//Enviar al servidor
	char *mensaje = (char *)malloc(50); 
	strcpy(mensaje,conexion->nombredb);
	char* elim=",eliminar,"; 
 	strncat(mensaje, elim,strlen(elim));
	char* cl=clave; 
 	strncat(mensaje, cl,strlen(cl));
	
	int p = 0;
	p = send(conexion->sockdf, mensaje, 50,0); 
	if (p < 0) {
		printf("Send error Cliente\n"); 
		return 0;
	}

	//Respuesta del servidor
	int n = 0;
	char buf[100] = "";
	char *mensajeok = "ok";
	n = recv(conexion->sockdf, buf, 100, 0);
	printf("\n ELiMINAR recibe :  > %s\n",buf); //Recibo ok
	if (n < 0){
		printf("Recv error Cliente\n"); 
		return 0;
	}	
	if (strcmp(buf,mensajeok) == 0) {

		//ACTUALIZAR INDICES
		put_val(conexion, clave, ":@T!");

		return 1;	
	}

	return 0;
}
