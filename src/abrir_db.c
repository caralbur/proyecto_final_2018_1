#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#include "logdb.h"
#include "indice.h"

//Recibe un objeto conexion. Con esto se informa al proceso logdb que abra la base de datos nombre_db.

int abrir_db(conexionlogdb *conexion, char *nombre_db){

	if(conexion == NULL || nombre_db == NULL){
		return 0;
	}

	int fdAR = open(nombre_db, O_RDWR, S_IRWXU); //El archivo  ya existe pero puede que no este en el de configuracion
	if(fdAR > 0){
		close(fdAR);
		
		int fd = open("./config", O_RDWR | O_CREAT | O_APPEND, S_IRWXU);
		if(fd <= 0){
			perror("No se pudo abrir el archivo de configuracion\n");
			return 0;	
		}

		char leer; int indice = 0; char linea[200] = ""; int r = 0;
		char *nombreBase = (char *)malloc(50);
		while(read(fd,&leer,1) != 0) {
			linea[r] = leer; 
			r++;
			if(leer == '\n'){				
				char* ch = strtok(linea, ","); int i = 0;

				while (ch != NULL) {
					if(i==0){
						nombreBase = ch;
					} 
					i++;
					ch = strtok(NULL, ",");
				} 
				
				if(strcmp(nombreBase,nombre_db)!=0){
					indice++;
					r = 0;
					memset(linea,0,200);
		
				} else if(strcmp(nombreBase,nombre_db)==0){
					break;
				}				
			}		
		}

		if(strcmp(nombreBase,nombre_db)!=0){
			char *ind = (char *)malloc(50);
			char *conve = (char *)malloc(50); 
			sprintf(conve, "%d", indice);

			strcat(ind, "./indice");
			strcat(ind, conve);
			strcat(ind, ".txt\n");

			char *sentLog = (char *)malloc(50);
			strncat(sentLog, nombre_db,strlen(nombre_db));
			strcat(sentLog, ",./,");
			strcat(sentLog, ind);

			int w = write(fd, sentLog, strlen(sentLog));
			if(w < 0){
				perror("No se pudo agregar el registro al archivo de configuracion\n");
			} 
		}
	}

	//Enviar al servidor
	char mensaje[50]="";
	strncat(mensaje, nombre_db,strlen(nombre_db));
	char* abrir=",abrir"; //mensaje=nombre_db,abrir
 	strncat(mensaje, abrir,strlen(abrir));
	int p = 0;
	p = send(conexion->sockdf, mensaje, 50,0); //envio el mensaje=nombre_db,abrir
	if (p < 0) {
		printf("Send error Cliente\n"); 
		return 0;
	}

	//Respuesta del servidor
	int n = 0;
	char buf[100] = "";
	char *mensajeok= "ok";
	n = recv(conexion->sockdf, buf, 100, 0);
	
	printf("\n ABRIR recibe :  > %s\n",buf); //recibo ok

	if (n < 0){
		printf("Recv error Cliente\n"); 
		return 0;
	}	
	if (strcmp(buf,mensajeok) == 0) {
		conexion->nombredb = nombre_db;

		//GENERAR INDICES DE ARCHIVO ORIGINAL
		int fd = open("./config", O_RDWR | O_APPEND, S_IRWXU);
		if(fd <= 0){
			perror("No se pudo abrir el archivo de configuracion\n");
			return 0;	
		}

		char leer; char linea[200] = ""; int r = 0;  int indice = 0;

		//Lee línea a línea y escribe en pantalla hasta el fin de fichero
		while(read(fd,&leer,1) != 0) { 
			linea[r] = leer; 
			r++;

			if(leer == '\n'){
				char *nombreBase = (char *)malloc(50);
				char *indiceDirec = (char *)malloc(50);
				char* ch = strtok(linea, ",");
				indice ++;
				int i=0;

				while (ch != NULL) {
					if(i==0){
						nombreBase = ch;
					} 
					if(i==2){
						indiceDirec = ch;
					} 
					i++;
					ch = strtok(NULL, ",");
				} 
				if(strcmp(conexion->nombredb,nombreBase)==0){
					close(fd);
					getIndice(nombreBase, indiceDirec);
					return 1;
		
				} else {
					r = 0;
					memset(linea,0,200);
				}

			}		

		}
		close(fd);
		return 1;	
	}

	return 0;		
}
	

