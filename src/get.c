#include <stdio.h>
#include "hashtable.h"

void *get(hashtable *tabla, char *clave){

	if(tabla == NULL || clave == NULL){
		return NULL;
	}

	unsigned long valorHash = 0;
	objeto *obj;

	unsigned char *key = (unsigned char *) clave;
	valorHash = hash(key)%(tabla->numeroBuckets);
	
	obj = tabla->buckets[valorHash];
	
	while(obj != NULL && obj->clave != NULL && strcmp(clave, obj->clave) > 0 ) {
		obj = obj->siguiente;
	}
	
	if(obj == NULL || obj->clave == NULL || strcmp(clave, obj->clave) != 0 ) {
		return NULL;

	} else {
		return obj->valor;
	}
}
