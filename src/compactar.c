#include <fcntl.h>
#include <error.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>

#include "hashtable.h"
#include "logdb.h"
#include "indice.h"

#define LINEA 256
#define ARREGLO 2
#define TAMANO 200

void compactar(conexionlogdb *conexion){

	if(conexion == NULL) {
		return ;
	}

	//No se ha abierto la DB
	if(strcmp(conexion->nombredb, "") == 0){
		return ;
	}

	//Enviar al servidor
	char mensaje[50]="";
	strncat(mensaje, conexion->nombredb,strlen(conexion->nombredb));
	char* comp=",compactar"; 
 	strncat(mensaje, comp,strlen(comp));
	int p = 0;
	p = send(conexion->sockdf, mensaje, 50,0);
	if (p < 0) {
		printf("Send error Cliente\n"); 
		return ;
	}

	//Respuesta del servidor
	int n = 0;
	char buf[100] = "";
	//char *mensajeok= "ok";
	n = recv(conexion->sockdf, buf, 100, 0);
	
	printf("\n COMPACTAR recibe :  > %s\n",buf); //recibo ok

	if (n < 0){
		printf("Recv error Cliente\n"); 
		return ;
	}	
	if (strcmp(buf,"ok") == 0) { 
		
		hashtable *tabla = crearHashTable(100000); 

		int fd = open(conexion->nombredb, O_RDONLY, S_IRWXU);
		char leer; char linea[200] = ""; int r = 0; 
		char *clave = (char *)malloc(50);
		char *valor = (char *)malloc(50);

		int m;
		while((m = read(fd,&leer,1)) != 0) { 
			if(leer != '\n'){
				linea[r] = leer; 
				r++;	
			}	
			else if(leer == '\n'){				
				char* ch = strtok(linea, ":");
				int j = 0;

				while (ch != NULL) {
					if(j == 0){
						clave = ch;
					} 
					if(j == 1){
						valor = ch;
					} 
					j++;
					ch = strtok(NULL, ":");
				} 
		
				put(tabla, strdup(clave), strdup(valor)); 
				
				r = 0;
				memset(linea,0,200);
			}
		} close(fd);

		int fd2 = open(conexion->nombredb, O_TRUNC, S_IRWXU); close(fd2);
		int fd3 = open(conexion->nombredb, O_WRONLY | O_APPEND, S_IRWXU); 

		int i = 0; 
		while(i < tabla->numeroBuckets){ 
			objeto *obj = tabla->buckets[i];
			if(obj != NULL &&  (char *)obj->clave != NULL){
				char *c = (char *) obj->clave; char *v = (char *) obj->valor;
				if(strcmp(c,"") != 0){
					write(fd3, c, strlen(c));				
					write(fd3, ":", strlen(":"));		
					write(fd3, v, strlen(v));
					write(fd3, "\n", strlen("\n"));
				}
				//obj = obj->siguiente;
			}
			
			i++;
		}
		close(fd3);	


		//ACTUALIZAR INDICES
		int fdOPEN = open("./config", O_RDWR | O_APPEND, S_IRWXU);
		if(fdOPEN <= 0){
			perror("No se pudo abrir el archivo de configuracion\n");
			return ;	
		}

		char leerO; char lineaO[200] = ""; int rO = 0;  int indiceO = 0;

		//Lee línea a línea y escribe en pantalla hasta el fin de fichero
		while(read(fdOPEN,&leerO,1) != 0) { 
			lineaO[rO] = leerO; 
			rO++;

			if(leerO == '\n'){
				char *nombreBase = (char *)malloc(50);
				char *indiceDirec = (char *)malloc(50);
				char* ch = strtok(lineaO, ",");
				indiceO ++;
				int i=0;

				while (ch != NULL) {
					if(i==0){
						nombreBase = ch;
					} 
					if(i==2){
						indiceDirec = ch;
					} 
					i++;
					ch = strtok(NULL, ",");
				} 
				if(strcmp(conexion->nombredb,nombreBase)==0){
					close(fd);
					getIndice(nombreBase, indiceDirec);
					return ;
		
				} else {
					r = 0;
					memset(lineaO,0,200);
				}

			}		

		}
		close(fdOPEN);
	
				
	}
	return ;

}
