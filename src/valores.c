#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"

void **valores(hashtable *tabla, int *conteo){
	
	if(tabla == NULL){
		return NULL;
	} else {
		*conteo = 0;
		void **arregloValores = malloc(sizeof(void *)*(tabla->elementos)); 

		int i = 0;

		while(i < tabla->numeroBuckets){ 
			objeto *obj = tabla->buckets[i];			
			if(obj != NULL && obj->valor != NULL){
				while(obj->siguiente != NULL){ 
					if(obj->siguiente != NULL && obj->valor != NULL){ 
						arregloValores[*(conteo)] = obj->valor;					
						*conteo = *conteo + 1;					
					}
					obj = obj->siguiente;						
				}	
			}
			i++;
		}

		//int j = 0;
		//printf("\nLos valores son: \n\n");
		//while(j < *conteo){
			//printf("%s\n",(char *) arregloValores[j]); 
		//	j++;
		//}

		printf("\nVariable conteo: %d\n", *conteo);		
		
		return arregloValores; 
	}

}
