#include <sys/types.h>          
#include <sys/stat.h>
#include <stdio.h>              
#include <stdlib.h>            
#include <stddef.h>             
#include <string.h>            
#include <unistd.h>           
#include <signal.h>           
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <ctype.h>
#include <fcntl.h>
#include <error.h>
#include <stdlib.h>
#include <unistd.h>

#define TAMANO 200

int servidor(int type, const struct sockaddr *addr, socklen_t alen, int solicitudes){ 

	int fd; int err = 0;
	
	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;
		
	if(bind(fd, addr, alen) < 0)
		goto errout;
		
	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){		
			if(listen(fd, solicitudes) < 0)
				goto errout;
	}
	return fd;


errout:
	err = errno;
	close(fd);
	errno = err;
	return (-1);
}

int main(int argc, char** argv){

	int sockfd;

	if(argc == 1){
		printf("Uso: ./cliente <directorio> <ip> <puerto>\n");
		exit(-1);
	}

	if(argc != 4){
		printf( "Cantidad de elementos erronea\n");
		exit(-1);
	}

	if(argc<4){
		printf("La cantidad de argumentos es invalida\n");
		exit(-1);
	}

	int puerto = atoi(argv[3]);

	struct sockaddr_in direccion;
	memset(&direccion, 0, sizeof(direccion));	
	direccion.sin_family = AF_INET;		
	direccion.sin_port = htons(puerto);		
	direccion.sin_addr.s_addr = inet_addr(argv[2]);

	if( (sockfd = servidor(SOCK_STREAM, (struct sockaddr *)&direccion, sizeof(direccion), 1000)) < 0){	
		printf("Error al inicializar el servidor\n");
	}

	int fd = 0;

	char *directorio = (char *)malloc(50); 
	strcpy(directorio,argv[1]);
	char buf[TAMANO] = {0};
	
	int clfd = -1;
	while(1){
		
		if(clfd == -1){
		  clfd = accept(sockfd, NULL, NULL);
		}

		//Escuchar al cliente
		memset(buf,0,TAMANO);
		int n = recv(clfd, buf, TAMANO, 0);
		/*if (n < 0) {	
			perror("Recv error Servidor\n"); 			
		}*/
		
		if(n == 0){ 
			close(clfd);
			clfd = -1; 
			continue;           
		}
		else{
			//Obtener nombre de DB, el metodo a usar, clave y valor (clave y/o valor para ciertos metodos)
			char *db; char *metodo; char *cl; char *val;
			char *cadena = strtok(buf,",");
			int i = 0;
				
			while(cadena != NULL){
				if(i == 0){
					db = cadena;
				}
				if(i == 1){
					metodo = cadena;
				}
				if(i == 2){
					cl = cadena;
				} else if(i < 2){ //Debido a que solo con else si llega al 3 el cl se eliminara
					cl = "...";
				}
				if(i == 3){
					val = cadena;
				} else {
					val = "...";
				}
				i++;
				cadena = strtok(NULL,",");
			} 					
			printf("%s %s %s %s\n", db,metodo,cl,val);	
			char *directorioConcat = (char *)malloc(50);
			strcpy(directorioConcat,directorio);
			if((strcmp(&directorioConcat[strlen(directorioConcat) - 1], "/") != 0)){ //Validar si el directorio no termine en /
				strcat(directorio,"/");
				char *r = strcat(directorioConcat,"/"); //Se le concatena al directorio el /
				strcat(r,db); //Se le concatena al directorio/ el nombre de la DB
			} else {
				strcat(directorioConcat,db); //Solo se concatena la database al ./
			}	

			//Acciones a realizar dependiendo del metodo enviado por el cliente
			if((strcmp(metodo, "crear") == 0)){ printf("%s\n",metodo);
				fd = creat(directorioConcat, S_IRWXU); //S_IRWXU -> Todos los permisos para el usuario
				if(fd <= 0){
					perror("No se pudo crear la DB\n");
				} else {
					send(clfd, directorio, 50, 0); 
				}		

			} 
			
			if((strcmp(metodo, "abrir") == 0)){ 
				fd = open(directorioConcat, O_WRONLY | O_APPEND, S_IRWXU); 
				if(fd <= 0){
					perror("No se pudo abrir la DB\n");
				} else { 
					send(clfd, "ok", 2, 0); 
				}
			} 

			if((strcmp(metodo, "put") == 0)){
				char *escribir = (char *)malloc(50);
				strcpy(escribir,cl);
				char *e = strcat(escribir,":");
				strcat(e,val);
				strcat(escribir,"\n");

				int w = write(fd,escribir,strlen(escribir));
				if(w < 0){
					perror("No se pudo agregar el par clave:valor a la DB\n");
				} else {	
					send(clfd, "ok", 2, 0); 
				}
			} 

			if((strcmp(metodo, "get") == 0)){
				fd = open(db, O_RDONLY | O_APPEND, S_IRWXU); 
				if(fd <= 0){
					perror("No se pudo abrir la DB\n");
				} else { 
					send(clfd, "ok", 2, 0); 
				}
			}
		 
			if((strcmp(metodo, "eliminar") == 0)){				
				char *escribir = (char *)malloc(50);
				strcpy(escribir,cl);
				strcat(escribir,":@T!");
				strcat(escribir,"\n");

				//int w = write(fd,escribir,strlen(escribir));
				if(escribir == NULL){
					perror("No se pudo eliminar la clave de la DB\n");
				} else {
					send(clfd, "ok", 2, 0); 
				}				
			} 

			if((strcmp(metodo, "cerrar") == 0)){				
				int c = close(fd);	
				if(c < 0){
					perror("No se pudo cerrar la DB\n");
				} else {	
					send(clfd, "ok", 2, 0);					
				}				
			} 

			if((strcmp(metodo, "compactar") == 0)){
				send(clfd, "ok", 2, 0); 
			}
		}
	}

	return 0;
}

