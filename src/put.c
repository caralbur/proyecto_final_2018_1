#include <stdio.h>
#include "hashtable.h"
#include <string.h>

void put(hashtable *tabla, char *clave, void *valor){

	if(tabla == NULL || clave == NULL || valor == NULL){
		
		printf("\nLo sentimos, al menos un argumento del metodo put resulto ser nulo.\n");		
		
		return ;
	}

	unsigned long valorHash = 0;
	objeto *obj=NULL;
	objeto *siguiente=NULL;
	objeto *anterior=NULL;

	unsigned char *key = (unsigned char *) clave; 
	valorHash = hash(key)%(tabla->numeroBuckets); //hash modulo de la cantidad de elementos en el bucket 
	
	siguiente=tabla->buckets[valorHash];

	//Si siguiente y su clave es diferente de NULL y la direccion de la clave es mayor a la clave de siguiente
	while( siguiente!=NULL && siguiente->clave!=NULL && strcmp(clave,siguiente->clave)>0 ){
		anterior=siguiente;
		siguiente=siguiente->siguiente;
	}
	
	if( siguiente !=NULL && siguiente->clave!=NULL && strcmp(clave,siguiente->clave)==0 ){
		free(siguiente->valor);	//Claves son iguales se sobreescribe el valor en dicha clave por eso primero libero el valor actual
		
		//siguiente->valor=strdup(valor); 
		siguiente->valor = valor; 
		
	}else{
		if( (obj=malloc(sizeof(objeto))) == NULL ){
			printf("No se dispone de mas memoria :C");
			return;
		}
		//if ((obj->clave=strdup(clave))==NULL){
		if ((obj->clave=clave) == NULL){
			printf("No se dispone de mas memoria :C");
			return;
		}
		//if ((obj->valor=strdup(valor))==NULL){
		if ((obj->valor=valor) == NULL){
			printf("No se dispone de mas memoria :C");
			return;
		}
		obj->siguiente=NULL;
		tabla->elementos++;

		if( siguiente==tabla->buckets[valorHash] ){
			obj->siguiente=siguiente;
			tabla->buckets[valorHash]=obj;
		}else if(siguiente==NULL){
			anterior->siguiente=obj;
		}else{
			obj->siguiente=siguiente;
			anterior->siguiente=obj;
		}

	}

}
