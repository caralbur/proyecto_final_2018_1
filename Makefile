#Escriba su makefiile en este archivo
#
# Carolina Burgos Pérez
# Luis Moya Larrea

#Si quieren llamar reglas adicionales, agregenlas como pre-requisitos a la regla all
#La regla logdb.so deben modificarla con su comando para generar dicho ejecutable.
#El archivo prueba.c esta vacio. NO deben llenarlo. Este se reemplazara con el codigo del profesor
#al probar su proyecto

#all: prueba

all: logdb.so libhastab.so prueba pruebaEstudiante server

#Prueba profesor
prueba: prueba.o indice.o logdb.so 
	gcc obj/prueba.o obj/indice.o  -llogdb -lhashtabprof -Llib/ -o bin/prueba

prueba.o: src/prueba.c
	gcc -Wall -c -I include/ src/prueba.c -o obj/prueba.o

#Prueba Estudiante
pruebaEstudiante: pruebaEstudiante.o indice.o logdb.so libhastab.so 
	gcc obj/pruebaEstudiante.o obj/indice.o -llogdb -lhastab -Llib/ -o bin/pruebaEstudiante

pruebaEstudiante.o: src/prueba.c 
	gcc -Wall -c -I include/ src/prueba.c -o obj/pruebaEstudiante.o

#Server 	
server: server.o
	gcc obj/server.o -o bin/logdb

server.o: src/server.c
	gcc -Wall -c src/server.c -o obj/server.o

#Indice
indice.o: src/indice.c
	gcc -Wall -c -fPIC -I include/ src/indice.c -o obj/indice.o

#Libreria de la DB
#logdb.so: src/logdb.c
logdb.so: conexionlogdb.o crear_db.o abrir_db.o put_val.o get_val.o eliminar.o cerrar_db.o compactar.o
	gcc -shared obj/conexionlogdb.o obj/crear_db.o obj/abrir_db.o obj/put_val.o obj/get_val.o obj/eliminar.o obj/cerrar_db.o obj/compactar.o -o lib/liblogdb.so

conexionlogdb.o: src/conexionlogdb.c
	gcc -Wall -c -fPIC -I include/ src/conexionlogdb.c -o obj/conexionlogdb.o

crear_db.o: src/crear_db.c
	gcc -Wall -c -fPIC -I include/ src/crear_db.c -o obj/crear_db.o

abrir_db.o: src/abrir_db.c
	gcc -Wall -c -fPIC -I include/ src/abrir_db.c -o obj/abrir_db.o

put_val.o: src/put_val.c
	gcc -Wall -c -fPIC -I include/ src/put_val.c -o obj/put_val.o

get_val.o: src/get_val.c
	gcc -Wall -c -fPIC -I include/ src/get_val.c -o obj/get_val.o

eliminar.o: src/eliminar.c
	gcc -Wall -c -fPIC -I include/ src/eliminar.c -o obj/eliminar.o

cerrar_db.o: src/cerrar_db.c
	gcc -Wall -c -fPIC -I include/ src/cerrar_db.c -o obj/cerrar_db.o

compactar.o: src/compactar.c
	gcc -Wall -c -fPIC -I include/ src/compactar.c -o obj/compactar.o

#Libreria del hashtable
libhastab.so: borrar.o claves.o contieneClave.o crearHashTable.o get.o numeroElementos.o put.o remover.o valores.o hash.o
	gcc -shared obj/borrar.o obj/claves.o obj/contieneClave.o obj/crearHashTable.o obj/get.o obj/numeroElementos.o obj/put.o obj/remover.o obj/valores.o obj/hash.o -o lib/libhastab.so

borrar.o: src/borrar.c
	gcc -Wall -c -fPIC -I include/ src/borrar.c -o obj/borrar.o

claves.o: src/claves.c
	gcc -Wall -c -fPIC -I include/ src/claves.c -o obj/claves.o

contieneClave.o: src/contieneClave.c
	gcc -Wall -c -fPIC -I include/ src/contieneClave.c -o obj/contieneClave.o

crearHashTable.o: src/crearHashTable.c
	gcc -Wall -c -fPIC -I include/ src/crearHashTable.c -o obj/crearHashTable.o

get.o: src/get.c
	gcc -Wall -c -fPIC -I include/ src/get.c -o obj/get.o

numeroElementos.o: src/numeroElementos.c
	gcc -Wall -c -fPIC -I include/ src/numeroElementos.c -o obj/numeroElementos.o

put.o: src/put.c
	gcc -Wall -c -fPIC -I include/ src/put.c -o obj/put.o

remover.o: src/remover.c
	gcc -Wall -c -fPIC -I include/ src/remover.c -o obj/remover.o

valores.o: src/valores.c
	gcc -Wall -c -fPIC -I include/ src/valores.c -o obj/valores.o

hash.o: src/hash.c
	gcc -Wall -c -fPIC src/hash.c -o obj/hash.o

.PHONY: clean
clean:
	rm bin/* obj/* lib/libhastab.so lib/logdb.so

