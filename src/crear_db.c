#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <unistd.h>

#include "logdb.h"

#define TAMANO 50

////Recibe un objeto conexion, y crear la db con el nombre dado. Devuelve 1 en exito, 0 en error (por ejemplo,
//ya existe un base de datos con ese nombre, error de E/S, etc).
				
int crear_db(conexionlogdb *conexion, char *nombre_db){
	
	if(conexion == NULL || nombre_db == NULL){
		return 0;
	}

	int fd = open("./config", O_RDWR | O_CREAT | O_APPEND, S_IRWXU);
	if(fd <= 0){
		perror("No se pudo abrir el archivo de configuracion\n");
		return 0;	
	}

	char leer; char linea[200] = ""; int r = 0;  int indice = 0;

	//Lee línea a línea y escribe en pantalla hasta el fin de fichero
	while(read(fd,&leer,1) != 0) { 
		linea[r] = leer; 
		r++;

		if(leer == '\n'){
			char *nombreBase = (char *)malloc(50);
			char* ch = strtok(linea, ",");
			indice ++;
			int i=0;

			while (ch != NULL) {
				if(i==0){
					//Obtengo los nombres de las bases de datos del archivo de configuracion
					nombreBase = ch;
				} 
				i++;
				ch = strtok(NULL, ",");
			} 
			printf("%s %s\n", nombreBase, nombre_db);
			if(strcmp(nombreBase,nombre_db)==0){
				return 0;
		
			} else {
				r = 0;
				memset(linea,0,200);
			}

		}		

	}
	
	//LA DB NO ESTA EN CONFIG
	char mensaje[50] = "";
	strncat(mensaje, nombre_db,strlen(nombre_db));
	char* crear=",crear";
 	strncat(mensaje, crear,strlen(crear)); 
	int p = 0;
	p = send(conexion->sockdf, mensaje, 50,0); 
	if (p < 0) {
		printf("Send error Cliente\n"); 
		return 0;
	}
	int n = 0;
	char buf[100] = "";
	n = recv(conexion->sockdf, buf, 100, 0);
	
	printf("\n CREAR recibe ruta:  > %s\n",buf); //recibo directorio
	if (n < 0){
		printf("Recv error Cliente\n"); 
	} else {

		char *ind = (char *)malloc(50);
		char *conve = (char *)malloc(50); 
		sprintf(conve, "%d", indice);
		strcat(ind, buf);
		strcat(ind, "indice");
		strcat(ind, conve);
		strcat(ind, ".txt\n");

		char *sentLog = (char *)malloc(50);
		strncat(sentLog, nombre_db,strlen(nombre_db));
		strcat(sentLog, ",");
		strcat(sentLog, buf);
		strcat(sentLog, ",");
		strcat(sentLog, ind);

		int w = write(fd, sentLog, strlen(sentLog));
		if(w < 0){
			perror("No se pudo agregar el registro al archivo de configuracion\n");
		} 
		return 1;	
	}		
	
	return 0;

} 

